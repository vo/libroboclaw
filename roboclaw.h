/**
 * RoboClaw Library (Header)
 *
 * Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
 * George Mason University - Autonomous Robotics Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY rcND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __LIBROBOCLAW_H__
#define __LIBROBOCLAW_H__

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <sys/time.h>
#include <stdint.h>
#include <stdarg.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>

// definitions and constants --------------------------------------------------
#define roboclaw_err_exit(err) { \
  printf("ERROR: %s: %s\n", __func__, err); \
  return -1; }

// instance data --------------------------------------------------------------
typedef struct 
{
	uint8_t swap[128];
	int fd;
	uint8_t address;
} RoboClawInstance;

// typedefs -------------------------------------------------------------------
typedef RoboClawInstance* RoboClawRef;

// low level serial functions -------------------------------------------------
int roboclaw_serial_open(int *fd, char* port);
int roboclaw_serial_configure(int fd, int baud);
int roboclaw_serial_write(int fd, unsigned char * buf, int n);
int roboclaw_serial_read_timeout(int fd, unsigned char * buf, unsigned int n,
		unsigned long secs, unsigned long usecs);

// initialization / deinitialization ------------------------------------------
int roboclaw_init(RoboClawRef rc, uint8_t address, int baud, char* port);
int roboclaw_close(RoboClawRef rc);

// basic communication --------------------------------------------------------
int roboclaw_write(RoboClawRef rc, int n);
int roboclaw_read(RoboClawRef rc, int n);
int roboclaw_trx(RoboClawRef rc, int out_bytes, int in_bytes);
int roboclaw_purge(RoboClawRef rc);
uint32_t roboclaw_unpack32(uint8_t * v);
uint16_t roboclaw_unpack16(uint8_t * v);
void roboclaw_pack32(uint8_t * dest, int n, ...); 
void roboclaw_pack16(uint8_t * dest, int n, ...);

// standard commands ----------------------------------------------------------
int roboclaw_drive_forward_M1(RoboClawRef rc, uint8_t value);
int roboclaw_drive_forward_M2(RoboClawRef rc, uint8_t value);
int roboclaw_drive_backward_M1(RoboClawRef rc, uint8_t value);
int roboclaw_drive_backward_M2(RoboClawRef rc, uint8_t value);
int roboclaw_drive_M1(RoboClawRef rc, uint8_t value);
int roboclaw_drive_M2(RoboClawRef rc, uint8_t value);
int roboclaw_set_min_main_voltage(RoboClawRef rc, uint8_t voltage);
int roboclaw_set_max_main_voltage(RoboClawRef rc, uint8_t voltage);

// mix commands ---------------------------------------------------------------
int roboclaw_drive_forward(RoboClawRef rc, uint8_t value);
int roboclaw_drive_backward(RoboClawRef rc, uint8_t value);
int roboclaw_turn_right(RoboClawRef rc, uint8_t value);
int roboclaw_turn_left(RoboClawRef rc, uint8_t value);
int roboclaw_drive(RoboClawRef rc, uint8_t value);
int roboclaw_turn(RoboClawRef rc, uint8_t value);

// version, status, and settings commands -------------------------------------
int roboclaw_version(RoboClawRef rc, char * result);
int roboclaw_read_voltage_batt(RoboClawRef rc, uint16_t * voltage);
int roboclaw_read_voltage_logic(RoboClawRef rc, uint16_t * voltage);
int roboclaw_set_logic_voltage_min(RoboClawRef rc, uint8_t voltage);
int roboclaw_set_logic_voltage_max(RoboClawRef rc, uint8_t voltage);
int roboclaw_read_motor_currents(RoboClawRef rc, uint16_t * m1cur, uint16_t * m2cur);
int roboclaw_read_PID_QPPS_M1(RoboClawRef rc, uint32_t * p, uint32_t * i, uint32_t * d, uint32_t * qpps);
int roboclaw_read_PID_QPPS_M2(RoboClawRef rc, uint32_t * p, uint32_t * i, uint32_t * d, uint32_t * qpps);
int roboclaw_read_PID_M1(RoboClawRef rc, uint32_t * p, uint32_t * i, uint32_t * d, uint32_t * max_i, uint32_t * deadzone, uint32_t * minpos, uint32_t * maxpos);
int roboclaw_read_PID_M2(RoboClawRef rc, uint32_t * p, uint32_t * i, uint32_t * d, uint32_t * max_i, uint32_t * deadzone, uint32_t * minpos, uint32_t * maxpos);
int roboclaw_set_voltage_cutoffs_main(RoboClawRef rc, uint16_t voltage_min, uint16_t voltage_max);
int roboclaw_set_voltage_cutoffs_logic(RoboClawRef rc, uint16_t voltage_min, uint16_t voltage_max);
int roboclaw_read_voltage_settings_main(RoboClawRef rc, uint16_t * min_voltage, uint16_t * max_voltage);
int roboclaw_read_voltage_settings_logic(RoboClawRef rc, uint16_t * min_voltage, uint16_t * max_voltage);
int roboclaw_read_temperature(RoboClawRef rc, uint16_t * temperature);
int roboclaw_read_error(RoboClawRef rc, uint8_t * err_bits);
int roboclaw_read_encoder_mode(RoboClawRef rc, uint8_t * m1mode, uint8_t * m2mode);
int roboclaw_set_encoder_mode_M1(RoboClawRef rc, uint8_t mode);
int roboclaw_set_encoder_mode_M2(RoboClawRef rc, uint8_t mode);
int roboclaw_write_settings_eeprom(RoboClawRef rc);

// advanced commands ----------------------------------------------------------
int roboclaw_set_PID_M1(RoboClawRef rc, uint32_t p, uint32_t i, uint32_t d, uint32_t qpps);
int roboclaw_set_PID_M2(RoboClawRef rc, uint32_t p, uint32_t i, uint32_t d, uint32_t qpps);
int roboclaw_read_encoder_M1(RoboClawRef rc, uint32_t * value, uint8_t * status);
int roboclaw_read_encoder_M2(RoboClawRef rc, uint32_t * value, uint8_t * status);
int roboclaw_read_speed_M1(RoboClawRef rc, uint32_t * speed, uint8_t * direction);
int roboclaw_read_speed_M2(RoboClawRef rc, uint32_t * speed, uint8_t * direction);
int roboclaw_read_speed_M1_highres(RoboClawRef rc, uint32_t * speed, uint32_t * direction);
int roboclaw_read_speed_M2_highres(RoboClawRef rc, uint32_t * speed, uint32_t * direction);
int roboclaw_reset_encoders(RoboClawRef rc);
int roboclaw_drive_duty_cycle_M1(RoboClawRef rc, uint16_t signed_duty);
int roboclaw_drive_duty_cycle_M2(RoboClawRef rc, uint16_t signed_duty);
int roboclaw_drive_duty_cycle(RoboClawRef rc, uint16_t signed_duty_m1, uint16_t signed_duty_m2);
int roboclaw_drive_speed_M1(RoboClawRef rc, uint32_t signed_speed);
int roboclaw_drive_speed_M2(RoboClawRef rc, uint32_t signed_speed);
int roboclaw_drive_speed(RoboClawRef rc, uint32_t signed_speed_m1, uint32_t signed_speed_m2);
int roboclaw_drive_accel_speed_M1(RoboClawRef rc, uint32_t accel, uint32_t qspeed);
int roboclaw_drive_accel_speed_M2(RoboClawRef rc, uint32_t accel, uint32_t qspeed);
int roboclaw_drive_accel_speed(RoboClawRef rc, uint32_t accel, uint32_t qspeed_m1, uint32_t qspeed_m2);
int roboclaw_drive_speed_dist_M1(RoboClawRef rc, uint32_t qspeed, uint32_t dist, uint8_t buffer);
int roboclaw_drive_speed_dist_M2(RoboClawRef rc, uint32_t qspeed, uint32_t dist, uint8_t buffer);
int roboclaw_drive_speed_dist(RoboClawRef rc, uint32_t qspeed_m1, uint32_t dist_m1, uint32_t qspeed_m2, uint32_t dist_m2, uint8_t buffer);
int roboclaw_drive_accel_speed_dist_M1(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t dist, uint8_t buffer);
int roboclaw_drive_accel_speed_dist_M2(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t dist, uint8_t buffer);
int roboclaw_drive_accel_speed_dist(RoboClawRef rc, uint32_t accel, uint32_t qspeed_m1, uint32_t dist_m1, uint32_t qspeed_m2, uint32_t dist_m2, uint8_t buffer);
int roboclaw_read_buffer_length(RoboClawRef rc, uint8_t * buffer_m1, uint8_t * buffer_m2);
int roboclaw_drive_speed_individual_accel(RoboClawRef rc, uint32_t accel_m1, uint32_t qspeed_m1, uint32_t accel_m2, uint32_t qspeed_m2);
int roboclaw_drive_speed_individual_accel_dist(RoboClawRef rc, uint32_t accel_m1, uint32_t qspeed_m1, uint32_t dist_m1, uint32_t accel_m2, uint32_t qspeed_m2, uint32_t dist_m2, uint8_t buffer);
int roboclaw_drive_duty_accel_M1(RoboClawRef rc, uint16_t signed_duty, uint16_t accel);
int roboclaw_drive_duty_accel_M2(RoboClawRef rc, uint16_t signed_duty, uint16_t accel);
int roboclaw_drive_duty_accel(RoboClawRef rc, uint16_t signed_duty_m1, uint32_t accel_m1, uint16_t signed_duty_m2, uint32_t accel_m2);
int roboclaw_set_pos_PID_M1(RoboClawRef rc, uint32_t p, uint32_t i, uint32_t d, uint32_t max_i, uint32_t deadzone, uint32_t min_pos, uint32_t max_pos);
int roboclaw_set_pos_PID_M2(RoboClawRef rc, uint32_t p, uint32_t i, uint32_t d, uint32_t max_i, uint32_t deadzone, uint32_t min_pos, uint32_t max_pos);
int roboclaw_drive_accel_speed_deccel_pos_M1(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t decel, uint32_t pos, uint8_t buffer);
int roboclaw_drive_accel_speed_deccel_pos_M2(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t decel, uint32_t pos, uint8_t buffer);
int roboclaw_drive_accel_speed_deccel_pos(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t decel, uint32_t pos, uint8_t buffer);

// utility --------------------------------------------------------------------
uint8_t roboclaw_checksum(RoboClawRef rc, int n);
int roboclaw_verify_checksum(RoboClawRef rc, int n);
int roboclaw_load_asciihex(RoboClawRef rc, const char * hex);

#endif