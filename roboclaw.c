/**
 * RoboClaw Library (Header)
 *
 * Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
 * George Mason University - Autonomous Robotics Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "roboclaw.h"

int roboclaw_serial_open(int *fd, char* port)
{
	// Open the serial port.
	// O_RDWR   : open for read/write
	// O_NOCTTY : do not cause terminal device to be controlling terminal
	// O_NDELAY : program ignores DCD line (else program sleeps until DCD)
	int fd_tmp = open(port, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd_tmp < 0) {
		fprintf(stderr, "roboclaw_serial_open: Cannot open %s - %s\n", port, strerror(
				errno));
		return fd_tmp;
	}
	// Configure file reading.
	// F_SETFL : set file status flags.
	*fd = fd_tmp;
	fcntl(*fd, F_SETFL, 0);
	return 0;
}

int roboclaw_serial_configure(int fd, int baud)
{
	struct termios options;
	int result = 0, result2 = 0;
	tcgetattr(fd, &options);

	speed_t baud_rate;

	// set baud
	switch(baud) {
		case 2400:
			baud_rate = B2400;
			break;
		case 9600:
			baud_rate = B9600;
			break;
		case 19200:
			baud_rate = B19200;
			break;
		case 38400:
		default:
			baud_rate = B38400;
			break;
	}
	result = cfsetispeed(&options, baud_rate);
	if (result < 0) {
		fprintf(stderr, "roboclaw_serial_configure: cfsetispeed() failed - %s\n",
				strerror(errno));
		return result;
	}
	result = cfsetospeed(&options, baud_rate);
	if (result < 0) {
		fprintf(stderr, "roboclaw_serial_configure: cfsetospeed() failed - %s\n",
				strerror(errno));
		return result;
	}

	// control options
	// 8 bits, no parity, all input, output, and line processing off
   	options.c_cflag = CS8 | CLOCAL | CREAD;
	options.c_lflag = 0;
	options.c_oflag = 0;
	options.c_iflag = IGNPAR | IGNBRK;

	// flush serial port
	result = tcflush(fd, TCIOFLUSH);
	if (result < 0) {
		fprintf(stderr, "roboclaw_serial_configure: first tcflush() failed - %s\n",
				strerror(errno));
		return result;
	}
	// set attributes for serial port
	result = tcsetattr(fd, TCSANOW, &options);
	if (result < 0) {
		fprintf(stderr, "roboclaw_serial_configure: tcsetattr() failed - %s\n",
				strerror(errno));
		return result;
	}
	// flush serial port again
	result = tcflush(fd, TCIOFLUSH);
	if (result < 0) {
		fprintf(stderr, "roboclaw_serial_configure: second tcflush() failed - %s\n",
				strerror(errno));
		return result;
	}
	return 0;
}

int roboclaw_serial_write(int fd, uint8_t * buf, int n)
{
	return (int) write(fd, buf, n);
}

int roboclaw_serial_read_timeout(int fd, uint8_t * buf, unsigned int n,
		unsigned long secs, unsigned long usecs)
{
	// set up fd
	fd_set set;
	FD_ZERO(&set);
	FD_SET(fd,&set);

	// set up timeout
	struct timeval timeout;
	timeout.tv_sec = secs;
	timeout.tv_usec = usecs;
	
	int i;
	int num_bytes = 0;
	while(num_bytes < n) {
		// wait for a data to come in
		int r = select(FD_SETSIZE, &set, NULL, NULL, &timeout);
		if (r < 0) // there was an error, return it.
			return r;
		if(r == 0) // no bytes ready, break out
			break;

		// read up to n bytes
		r = read(fd, buf+num_bytes, n);
		if (r < 0) // read error?
			return r;

		num_bytes += r;
	}
	return num_bytes;
}

/*-----------------------------------------------------------------------------
 * Load the given data (from ASCII hex string) into swap
 * Returns number of bytes read into swap
 */
int roboclaw_load_asciihex(RoboClawRef rc, const char * hex)
{
	assert(rc);
	int i, j;
	int len = strlen(hex);

	// copy just the hex characters into a temporary buffer
	char hexstr[len];
	for (i = 0, j = 0; i < len; i++)
		if (isxdigit(hex[i]))
			hexstr[j++] = hex[i];
	hexstr[j] = '\0';
	len = j;
	int bytelen = len / 2;
	char buf[2];

	// clear swap
	for (i = 0; i < 128; i++)
		rc->swap[i] = 0;

	// convert hex chars to bytes
	for (i = 0, j = 0; i < len; i += 2, j++) {
		buf[0] = hexstr[i];
		buf[1] = hexstr[i + 1];
		rc->swap[j] = strtol(buf, NULL, 16);
	}

	return bytelen;
}

/*-----------------------------------------------------------------------------
 * Compute checksum for n bytes (swap[0] to swap[n-1]).
 * Returns checksum value.
 */
uint8_t roboclaw_checksum(RoboClawRef rc, int n)
{
	assert(rc);
	int i;
	unsigned int sum = 0;
	for (i = 0; i < n; i++) 
		sum += rc->swap[i];
	return (((uint8_t)sum) & 0x7F);
}

/*-----------------------------------------------------------------------------
 * Verify checksum for n bytes (swap[0] to swap[n-1]).
 * Returns 0 if correct, < 0 if incorrect
 */
int roboclaw_verify_checksum(RoboClawRef rc, int n)
{
	assert(rc);
	int i;
	unsigned int sum = 0;
	for (i = 0; i < n; i++)
		sum += rc->swap[i];
	return (sum & 0x7F) == rc->swap[n] ? 0 : -1;
}

/*-----------------------------------------------------------------------------
 * Open / Initialize the RoboClawInstance
 * Accepts port name and baud rate
 * baud: the baud rate (2400, 9600, 19200, 38400)
 * Returns 0 if successful, error code otherwise.
 */
int roboclaw_init(RoboClawRef rc, uint8_t address, int baud, char* port)
{
	assert(rc);
	int i;

	// set the address
	rc->address = address;

	// open the serial port
	if(roboclaw_serial_open(&rc->fd, port) < 0)
		roboclaw_err_exit("Could not open serial port. Exiting.");

	// configure serial port
	if(roboclaw_serial_configure(rc->fd, baud) < 0)
		roboclaw_err_exit("Could not configure baud rate. Exiting.");

	return 0;
}

/*-----------------------------------------------------------------------------
 * Close / Deinitialize the RoboClawInstance.
 * This consists mainly of closing the serial port gracefully.
 * Returns 0 if successful, < 0 if error
 */
int roboclaw_close(RoboClawRef rc)
{
	assert(rc);

	// close file desccriptor
	if(close(rc->fd) < 0)
		roboclaw_err_exit("Could not close the file");

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read n bytes from the RoboClaw. Waits at most 1 second for the bytes.
 * Returns < 0: error
 * Returns >= 0: number of bytes read
 */
int roboclaw_read(RoboClawRef rc, int n)
{
	assert(rc);
	int i;
	if ((i = roboclaw_serial_read_timeout(rc->fd, rc->swap, n, 0, 100000)) < 0)
		roboclaw_err_exit("Error occurred during serial read.");
	return i;
}

/*-----------------------------------------------------------------------------
 * Write n bytes from the swap to the RoboClaw.
 * Returns >0 number of bytes written, < 0 if error
 */
int roboclaw_write(RoboClawRef rc, int n)
{
	assert(rc);
	int i;
	if ((i = roboclaw_serial_write(rc->fd, rc->swap, n)) < 0)
		roboclaw_err_exit("Error occurred during serial write.");
	return i;
}

/*-----------------------------------------------------------------------------
 * Flush the TX and RX buffers.
 * Returns 0 if all good.
 */
int roboclaw_purge(RoboClawRef rc) {
	assert(rc);
	int i;
	// flush serial port
	i = tcflush(rc->fd, TCIOFLUSH);
	if (i < 0) {
		fprintf(stderr, "trx: tcflush() failed - %s\n",
				strerror(errno));
		return i;
	}
	return 0;
}

/*-----------------------------------------------------------------------------
 * Transaction template: Purge, then send out_bytes, then receive in_bytes
 * Returns < 0: error
 * Returns >= 0: number of bytes read
 */
int roboclaw_trx(RoboClawRef rc, int out_bytes, int in_bytes)
{
	assert(rc);
	int i;
	int j;
	int k;

	// purge
	if ((i = roboclaw_purge(rc)) < 0)
		return i;

	// write
	if ((i = roboclaw_write(rc, out_bytes)) < 0)
		return i;

	// read
	if ((i = roboclaw_read(rc, in_bytes)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read a 32-bit val out of an array of uint8_t
 * v: where to start reading.
 */
uint32_t roboclaw_unpack32(uint8_t * v) 
{
	return ((uint32_t)v[0] << 24) | ((uint32_t)v[1] << 16) | ((uint32_t)v[2] << 8) | v[3];
}

/*-----------------------------------------------------------------------------
 * Read a 16-bit val out of an array of uint8_t
 * v: where to start reading.
 */
uint16_t roboclaw_unpack16(uint8_t * v) 
{
	return ((uint32_t)v[0] << 8) | v[1];
}

/*-----------------------------------------------------------------------------
 * Write a 32-bit value into an array of uint8_t
 * dest: where to start writing
 * n: the number of values that will follow
 * [...]: the 32-bit values to write, expected to be uint32_t
 */
void roboclaw_pack32(uint8_t * dest, int n, ...) 
{
	va_list args;
	va_start(args, n);

	int i;
	for(i = 0; i < n; ++i) 
	{
		uint32_t val = va_arg(args, uint32_t);	
		dest[0] = (uint8_t)((val & 0xFF000000) >> 24);
		dest[1] = (uint8_t)((val & 0x00FF0000) >> 16);
		dest[2] = (uint8_t)((val & 0x0000FF00) >> 8);
		dest[3] = (uint8_t)(val & 0x000000FF); 
		dest += sizeof(uint32_t);
	}

	va_end(args);
}

/*-----------------------------------------------------------------------------
 * Write a 16-bit value into an array of uint8_t
 * dest: where to start writing
 * n: the number of values that will follow
 * [...]: the 16-bit values to write, expected to be uint16_t
 */
void roboclaw_pack16(uint8_t * dest, int n, ...) 
{
	va_list args;
	va_start(args, n);

	int i;
	for(i = 0; i < n; ++i) 
	{
		uint16_t val = va_arg(args, int);
		dest[0] = (uint8_t)((val & 0x0000FF00) >> 8);
		dest[1] = (uint8_t)(val & 0x000000FF);
		dest += sizeof(uint16_t); 
	}

	va_end(args);
}

/*-----------------------------------------------------------------------------
 * Drive motor 1 forward. 
 * value: 0 - 127; 127 = full-value forward, 64 = half-value, 0 = full-stop.
 * Return: 0 if success, < 0 if failure.
 */ 
int roboclaw_drive_forward_M1(RoboClawRef rc, uint8_t value) 
{
	assert(rc);
	int i;

	// command:
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 0; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	if ((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Drive motor 1 backward. 
 * value: 0 - 127; 127 = full-value backward, 64 = half-value, 0 = full-stop.
 * Return: 0 if success, < 0 if failure.
 */ 
int roboclaw_drive_backward_M1(RoboClawRef rc, uint8_t value) 
{
	assert(rc);
	int i;

	// command:
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 1; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	if ((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Drive motor 2 forward. 
 * value: 0 - 127; 127 = full-value forward, 64 = half-value, 0 = full-stop.
 * Return: 0 if success, < 0 if failure.
 */ 
int roboclaw_drive_forward_M2(RoboClawRef rc, uint8_t value) 
{
	assert(rc);
	int i;

	// command:
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 4; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	if ((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Drive motor 2 backward. 
 * value: 0 - 127; 127 = full-value backward, 64 = half-value, 0 = full-stop.
 * Return: 0 if success, < 0 if failure.
 */ 
int roboclaw_drive_backward_M2(RoboClawRef rc, uint8_t value) 
{
	assert(rc);
	int i;

	// command:
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 5; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	if ((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Drive motor 1 (Signed value) 
 * value: 0 - 127; 127 = full-backward, 64 = zero, 0 = full-forward.
 * Return: 0 if success, < 0 if failure.
 */ 
int roboclaw_drive_M1(RoboClawRef rc, uint8_t value) 
{
	assert(rc);
	int i;

	// command:
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 6; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	if ((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Drive motor 2 (Signed value) 
 * value: 0 - 127; 127 = full-backward, 64 = zero, 0 = full-forward.
 * Return: 0 if success, < 0 if failure.
 */ 
int roboclaw_drive_M2(RoboClawRef rc, uint8_t value) 
{
	assert(rc);
	int i;

	// command:
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 7; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	if ((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Set minimum main voltage
 * voltage: 0-120 (6V-30V). If battery drops below this value, the RoboClaw
 *   will shut down. must be set upon each power-up (not saved). Voltage is
 *   in 0.2V increments. Formula: (Desired Volts - 6) * 5 = Value. Examples:
 *   6V = 0, 8V = 10, 11V = 25. 
 * Return: 0 if success, < 0 if failure.
 */  
int roboclaw_set_min_main_voltage(RoboClawRef rc, uint8_t voltage)
{
	assert(rc);
	int i;

	rc->swap[0] = rc->address; // address
	rc->swap[1] = 2; // command
	rc->swap[2] = voltage; // voltage
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum
	
	if((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Set maximum main voltage
 * voltage: 0-154 (0V-30V). If you are using a battery of any type you can 
 *   ignore this setting. During regenerative breaking a back voltage is 
 *   applied to charge the battery. When using an ATX type power supply if it 
 *   senses anything over 16V it will shut down. By setting the maximum voltage 
 *   level, RoboClaw before exceeding it will go into hard breaking mode until 
 *   the voltage drops below the maximum value set. The formula for calculating
 *   the voltage is: Desired Volts x 5.12 = Value. Examples of valid values 
 *   are 12V = 62, 16V = 82 and 24V = 123.
 * Return: 0 if success, < 0 if failure.
 */
int roboclaw_set_max_main_voltage(RoboClawRef rc, uint8_t voltage)
{
	assert(rc);
	int i;

	rc->swap[0] = rc->address; // address
	rc->swap[1] = 3; // command
	rc->swap[2] = voltage; // voltage
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum
	
	if((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Mix Drive Forward.
 * value: 0-127, 0 = full stop, 127 = full forward.
 * Return: 0 if success, < 0 if failure.
 */
int roboclaw_drive_forward(RoboClawRef rc, uint8_t value)
{
	assert(rc);
	int i;

	rc->swap[0] = rc->address; // address
	rc->swap[1] = 8; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum
	
	if((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Mix Drive Backward.
 * value: 0-127, 0 = full stop, 127 = full backward.
 * Return: 0 if success, < 0 if failure.
 */
int roboclaw_drive_backward(RoboClawRef rc, uint8_t value)
{
	assert(rc);
	int i;

	rc->swap[0] = rc->address; // address
	rc->swap[1] = 8; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum
	
	if((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Mix Turn Right.
 * value: 0-127, 0 = full stop, 127 = full turn.
 * Return: 0 if success, < 0 if failure.
 */
int roboclaw_turn_right(RoboClawRef rc, uint8_t value)
{
	assert(rc);
	int i;

	rc->swap[0] = rc->address; // address
	rc->swap[1] = 10; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum
	
	if((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Mix Turn Left.
 * value: 0-127, 0 = full stop, 127 = full turn.
 * Return: 0 if success, < 0 if failure.
 */
int roboclaw_turn_left(RoboClawRef rc, uint8_t value)
{
	assert(rc);
	int i;

	rc->swap[0] = rc->address; // address
	rc->swap[1] = 11; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum
	
	if((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Mix Drive (7-bit)
 * value: 0-127, 0 = full backward, 64 = stop, 127 = full forward.
 * Return: 0 if success, < 0 if failure.
 */
int roboclaw_drive(RoboClawRef rc, uint8_t value)
{
	assert(rc);
	int i;

	rc->swap[0] = rc->address; // address
	rc->swap[1] = 12; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum
	
	if((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Mix Turn (7-bit)
 * value: 0-127, 0 = full left, 64 = stop, 127 = full right.
 * Return: 0 if success, < 0 if failure.
 */
int roboclaw_turn(RoboClawRef rc, uint8_t value) 
{
	assert(rc);
	int i;

	rc->swap[0] = rc->address; // address
	rc->swap[1] = 13; // command
	rc->swap[2] = value; // value
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum
	
	if((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Get the version string (Command 21)
 * result: Where the version string will be stored. Needs at least 32 bytes.
 * Returns 0: Success.
 * Returns < 0: Error. 
 */
int roboclaw_version(RoboClawRef rc, char * result)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 21; // command

	// send 2, expect 32 in response
	if ((i = roboclaw_trx(rc, 2, 32)) < 0)
		return i;

	memcpy(result, rc->swap, 32);

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read Main Battery Voltage Level
 * Read the main battery voltage level connected to B+ and B- terminals. 
 * The voltage is returned in 10ths of a volt.
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_voltage_batt(RoboClawRef rc, uint16_t * voltage)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 24; // command

	// send 2, expect 3 in response
	if ((i = roboclaw_trx(rc, 2, 3)) < 0)
		return i;

	// pack response into uint16_t
	*voltage = roboclaw_unpack16(rc->swap);

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read Logic Battery Voltage Level
 * Read a logic battery voltage level connected to LB+ and LB- terminals. 
 * The voltage is returned in 10ths of a volt.
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_voltage_logic(RoboClawRef rc, uint16_t * voltage)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 25; // command

	// send 2, expect 3 in response
	if ((i = roboclaw_trx(rc, 2, 3)) < 0)
		return i;

	// pack response into uint16_t
	*voltage = roboclaw_unpack16(rc->swap);

	return 0;
}

/*-----------------------------------------------------------------------------
 * Set Minimum Logic Voltage Level
 * Sets logic input (LB- / LB+) minimum voltage level. If the battery voltages 
 * drops below the set voltage level RoboClaw will shut down. The value is 
 * cleared at start up and must set after each power up. The voltage is set in 
 * .2 volt increments. A value of 0 sets the minimum value allowed which is 3V.
 * The valid data range is 0 - 120 (6V - 28V). The formula for calculating the 
 * voltage is: (Desired Volts - 6) x 5 = Value. Examples of valid values are 
 * 3V = 0, 8V = 10 and 11V = 25.
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_set_logic_voltage_min(RoboClawRef rc, uint8_t voltage)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 26; // get version command
	rc->swap[2] = voltage; // voltage
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	// send 3
	if ((i = roboclaw_write(rc, 3)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Set Maximum Logic Voltage Level
 * Sets logic input (LB- / LB+) maximum voltage level. The valid data range is 
 * 0 - 144 (0V - 28V). By setting the maximum voltage level RoboClaw will go 
 * into shut down and requires a hard reset to recovers. The formula for 
 * calculating the voltage is: Desired Volts x 5.12 = Value. Examples of valid
 * values are 12V = 62, 16V = 82 and 24V = 123.
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_set_logic_voltage_max(RoboClawRef rc, uint8_t voltage)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 27; // get version command
	rc->swap[2] = voltage; // voltage
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	// send 3
	if ((i = roboclaw_write(rc, 3)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read Motor Currents
 * Read the current draw from each motor in 10ma increments.
 * m1cur: motor 1 current, in 10ma units
 * m2cur: motor 2 current, in 10ma units
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_motor_currents(RoboClawRef rc, uint16_t * m1cur, uint16_t * m2cur)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 49; // command

	// send 2, expect 5 in response
	if ((i = roboclaw_trx(rc, 2, 5)) < 0)
		return i;

	*m1cur = roboclaw_unpack16(rc->swap);
	*m2cur = roboclaw_unpack16(rc->swap+2);

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read Motor 1 P, I, D and QPPS Settings
 * Read the PID and QPPS Settings.
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_PID_QPPS_M1(RoboClawRef rc, uint32_t * p, uint32_t * i, uint32_t * d, uint32_t * qpps)
{
	assert(rc);
	int ret;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 55; // get version command

	// send 2, expect 17 in response
	if ((ret = roboclaw_trx(rc, 2, 17)) < 0)
		return ret;

  	*p = roboclaw_unpack32(rc->swap);
  	*i = roboclaw_unpack32(rc->swap+4);
  	*d = roboclaw_unpack32(rc->swap+8);
  	*qpps = roboclaw_unpack32(rc->swap+12);

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read Motor 2 P, I, D and QPPS Settings
 * Read the PID and QPPS Settings.
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_PID_QPPS_M2(RoboClawRef rc, uint32_t * p, uint32_t * i, uint32_t * d, uint32_t * qpps)
{
	assert(rc);
	int ret;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 56; // get version command

	// send 2, expect 17 in response
	if ((ret = roboclaw_trx(rc, 2, 17)) < 0)
		return ret;

  	*p = roboclaw_unpack32(rc->swap);
  	*i = roboclaw_unpack32(rc->swap+4);
  	*d = roboclaw_unpack32(rc->swap+8);
  	*qpps = roboclaw_unpack32(rc->swap+12);

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read Motor 1 PID Settings
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_PID_M1(RoboClawRef rc, uint32_t * p, uint32_t * i, uint32_t * d, uint32_t * max_i, uint32_t * deadzone, uint32_t * minpos, uint32_t * maxpos)
{
	assert(rc);
	int ret;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 63; // get version command

	// send 2, expect 29 in response
	if ((ret = roboclaw_trx(rc, 2, 29)) < 0)
		return ret;

  	*p = roboclaw_unpack32(rc->swap);
  	*i = roboclaw_unpack32(rc->swap+4);
  	*d = roboclaw_unpack32(rc->swap+8);
  	*max_i = roboclaw_unpack32(rc->swap+12);
  	*deadzone = roboclaw_unpack32(rc->swap+16);
  	*minpos = roboclaw_unpack32(rc->swap+20);
  	*maxpos = roboclaw_unpack32(rc->swap+24);

  	return 0;
}

/*-----------------------------------------------------------------------------
 * Read Motor 2 PID Settings
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_PID_M2(RoboClawRef rc, uint32_t * p, uint32_t * i, uint32_t * d, uint32_t * max_i, uint32_t * deadzone, uint32_t * minpos, uint32_t * maxpos)
{
	assert(rc);
	int ret;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 64; // get version command

	// send 2, expect 29 in response
	if ((ret = roboclaw_trx(rc, 2, 29)) < 0)
		return ret;

  	*p = roboclaw_unpack32(rc->swap);
  	*i = roboclaw_unpack32(rc->swap+4);
  	*d = roboclaw_unpack32(rc->swap+8);
  	*max_i = roboclaw_unpack32(rc->swap+12);
  	*deadzone = roboclaw_unpack32(rc->swap+16);
  	*minpos = roboclaw_unpack32(rc->swap+20);
  	*maxpos = roboclaw_unpack32(rc->swap+24);

  	return 0;
}

/*-----------------------------------------------------------------------------
 * Set main battery voltage cutoffs, min and max
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_set_voltage_cutoffs_main(RoboClawRef rc, uint16_t voltage_min, uint16_t voltage_max)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 57; // command
	roboclaw_pack16(rc->swap + 2, 2, voltage_min, voltage_max);
	rc->swap[6] = roboclaw_checksum(rc, 6); // checksum

	// send 7
	if ((i = roboclaw_write(rc, 7)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Set main battery voltage cutoffs, min and max
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_set_voltage_cutoffs_logic(RoboClawRef rc, uint16_t voltage_min, uint16_t voltage_max)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 58; // command
	roboclaw_pack16(rc->swap + 2, 2, voltage_min, voltage_max);
	rc->swap[6] = roboclaw_checksum(rc, 6); // checksum

	// send 7
	if ((i = roboclaw_write(rc, 7)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read Main Battery Voltage Settings
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_voltage_settings_main(RoboClawRef rc, uint16_t * min_voltage, uint16_t * max_voltage)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 59; // get version command

	// send 2, expect 5 in response
	if ((i = roboclaw_trx(rc, 2, 5)) < 0)
		return i;

	*min_voltage = roboclaw_unpack16(rc->swap);
	*max_voltage = roboclaw_unpack16(rc->swap+2);

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read Logic Battery Voltage Settings 
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_voltage_settings_logic(RoboClawRef rc, uint16_t * min_voltage, uint16_t * max_voltage)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 60; // get version command

	// send 2, expect 5 in response
	if ((i = roboclaw_trx(rc, 2, 5)) < 0)
		return i;

	*min_voltage = roboclaw_unpack16(rc->swap);
	*max_voltage = roboclaw_unpack16(rc->swap+2);

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read the board temperature in 0.1 degree increments.
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_temperature(RoboClawRef rc, uint16_t * temperature)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 82; // get version command

	// send 2, expect 3 in response
	if ((i = roboclaw_trx(rc, 2, 3)) < 0)
		return i;

	*temperature = roboclaw_unpack16(rc->swap);

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read error state
 * Masks:
 * Normal: 0x00
 * M1 OverCurrent: 0x01
 * M2 OverCurrent: 0x02
 * E-Stop: 0x04
 * Temperature: 0x08
 * Main Battery High: 0x10
 * Main Battery Low: 0x20
 * Logic Battery High: 0x40
 * Logic Battery Low: 0x80
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_error(RoboClawRef rc, uint8_t * err_bits)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 90; // get version command

	// send 2, expect 2 in response
	if ((i = roboclaw_trx(rc, 2, 2)) < 0)
		return i;

	*err_bits = rc->swap[0];

	return 0;
}

/*-----------------------------------------------------------------------------
 * Read the encoder mode for both motors.
 * Bit 7: Enable encoder, Bit 0: Quadrature (0) / Absolute (1)
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_read_encoder_mode(RoboClawRef rc, uint8_t * m1mode, uint8_t * m2mode)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 91; // get version command

	// send 2, expect 3 in response
	if ((i = roboclaw_trx(rc, 2, 3)) < 0)
		return i;

	*m1mode = rc->swap[0];
	*m2mode = rc->swap[1];

	return 0;
}

/*-----------------------------------------------------------------------------
 * Set the encoder mode for M1. 
 * Bit 7: Enable encoder, Bit 0: Quadrature (0) / Absolute (1)
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_set_encoder_mode_M1(RoboClawRef rc, uint8_t mode)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 92; // get version command
	rc->swap[2] = mode;
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	// send 4
	if ((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Set the encoder mode for M2. 
 * Bit 7: Enable encoder, Bit 0: Quadrature (0) / Absolute (1)
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_set_encoder_mode_M2(RoboClawRef rc, uint8_t mode)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 93; // get version command
	rc->swap[2] = mode;
	rc->swap[3] = roboclaw_checksum(rc, 3); // checksum

	// send 4
	if ((i = roboclaw_write(rc, 4)) < 0)
		return i;

	return 0;
}

/*-----------------------------------------------------------------------------
 * Write current settings to eeprom.
 * Return: 0: Success. 
 * Returns < 0: Error.
 */
int roboclaw_write_settings_eeprom(RoboClawRef rc)
{
	assert(rc);
	int i;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 94; // get version command

	// send 2
	if ((i = roboclaw_write(rc, 2)) < 0)
		return i;

	return 0;
}

/*----------------------------------------------------------------------------
 * Set PID Constants M1
 * Several motor and quadrature combinations can be used with RoboClaw. In some
 * cases the default PID values will need to be tuned for the systems being 
 * driven. This gives greater flexibility in what motor and encoder 
 * combinations can be used. The RoboClaw PID system consist of four constants
 * starting with QPPS, P = Proportional, I= Integral and D= Derivative. 
 * The defaults values are:
 * QPPS = 44000
 * P = 0x00010000
 * I = 0x00008000
 * D = 0x00004000
 * QPPS is the speed of the encoder when the motor is at 100% power. 
 * P, I, D are the default values used after a reset. 
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_set_PID_M1(RoboClawRef rc, uint32_t p, uint32_t i, uint32_t d, uint32_t qpps) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 28; // command
	roboclaw_pack32(rc->swap + 2, 4, p, i, d, qpps);
	rc->swap[18] = roboclaw_checksum(rc, 18); // checksum

	// send 19
	if ((result = roboclaw_write(rc, 19)) < 0)
		return result;

	return 0;
}

/*----------------------------------------------------------------------------
 * 
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_set_PID_M2(RoboClawRef rc, uint32_t p, uint32_t i, uint32_t d, uint32_t qpps) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 29; // command
	roboclaw_pack32(rc->swap + 2, 4, p, i, d, qpps);
	rc->swap[18] = roboclaw_checksum(rc, 18); // checksum

	// send 19
	if ((result = roboclaw_write(rc, 19)) < 0)
		return result;

	return 0;
}


/*----------------------------------------------------------------------------
 * Read Encoder M1.
 * The command will return 6 bytes. Byte 1,2,3 and 4 make up a long variable 
 * which is received MSB first and represents the current count which can be 
 * any value from 0 - 4,294,967,295. Each pulse from the quadrature encoder 
 * will increment or decrement the counter depending on the direction of 
 * rotation. Byte 5 is the status byte for M1 decoder. It tracks counter 
 * underflow, direction, overflow and if the encoder is operational. The byte 
 * value represents:	
 * Bit0 - Counter Underflow (1= Underflow Occurred, Clear After Reading)
 * Bit1 - Direction (0 = Forward, 1 = Backwards)
 * Bit2 - Counter Overflow (1= Underflow Occurred, Clear After Reading)
 * Bit3 - Reserved
 * Bit4 - Reserved
 * Bit5 - Reserved
 * Bit6 - Reserved
 * Bit7 - Reserved
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_read_encoder_M1(RoboClawRef rc, uint32_t * value, uint8_t * status) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 16; // command

	// send 2, expect 6
	if ((result = roboclaw_trx(rc, 2, 6)) < 0)
		return result;

	*value = roboclaw_unpack32(rc->swap);
	*status = rc->swap[4];

	return 0;
}


/*----------------------------------------------------------------------------
 * Read Encoder M2
 * The command will return 6 bytes. Byte 1,2,3 and 4 make up a long variable 
 * which is received MSB first and represents the current count which can be 
 * any value from 0 - 4,294,967,295. Each pulse from the quadrature encoder 
 * will increment or decrement the counter depending on the direction of 
 * rotation. Byte 5 is the status byte for M1 decoder. It tracks counter 
 * underflow, direction, overflow and if the encoder is operational. The byte 
 * value represents:	
 * Bit0 - Counter Underflow (1= Underflow Occurred, Clear After Reading)
 * Bit1 - Direction (0 = Forward, 1 = Backwards)
 * Bit2 - Counter Overflow (1= Underflow Occurred, Clear After Reading)
 * Bit3 - Reserved
 * Bit4 - Reserved
 * Bit5 - Reserved
 * Bit6 - Reserved
 * Bit7 - Reserved
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_read_encoder_M2(RoboClawRef rc, uint32_t * value, uint8_t * status) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 17; // command

	// send 2, expect 6
	if ((result = roboclaw_trx(rc, 2, 6)) < 0)
		return result;

	*value = roboclaw_unpack32(rc->swap);
	*status = rc->swap[4];

	return 0;
}

/*----------------------------------------------------------------------------
 * Read Speed M1
 * The command will return 6 bytes. Byte 1,2,3 and 4 make up a long variable 
 * which is received MSB first and is the current ticks per second which can 
 * be any value from 0 - 4,294,967,295. Byte 5 is the direction (0 – forward, 
 * 1 - backward).
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_read_speed_M1(RoboClawRef rc, uint32_t * speed, uint8_t * direction) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 18; // command

	// send 2, expect 6
	if ((result = roboclaw_trx(rc, 2, 6)) < 0)
		return result;

	*speed = roboclaw_unpack32(rc->swap);
	*direction = rc->swap[4];

	return 0;
}

/*----------------------------------------------------------------------------
 * Read Speed M2
 * The command will return 6 bytes. Byte 1,2,3 and 4 make up a long variable 
 * which is received MSB first and is the current ticks per second which can 
 * be any value from 0 - 4,294,967,295. Byte 5 is the direction (0 – forward, 
 * 1 - backward).
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_read_speed_M2(RoboClawRef rc, uint32_t * speed, uint8_t * direction) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 19; // command

	// send 2, expect 6
	if ((result = roboclaw_trx(rc, 2, 6)) < 0)
		return result;

	*speed = roboclaw_unpack32(rc->swap);
	*direction = rc->swap[4];

	return 0;
}

/*----------------------------------------------------------------------------
 * Read Speed M1 (High Resolution)
 * Read the current pulse per 125th of a second. This is a high resolution 
 * version of command 18 and 19. Command 30 can be used to make a independent 
 * PID routine. The resolution of the command is required to create a PID 
 * routine using any microcontroller or PC used to drive RoboClaw.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_read_speed_M1_highres(RoboClawRef rc, uint32_t * speed, uint32_t * direction) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 30; // command

	// send 2, expect 6
	if ((result = roboclaw_trx(rc, 2, 6)) < 0)
		return result;

	*speed = roboclaw_unpack32(rc->swap);
	*direction = rc->swap[4];

	return 0;
}

/*----------------------------------------------------------------------------
 * Read Speed M2 (High Resolution)
 * Read the current pulse per 125th of a second. This is a high resolution 
 * version of command 18 and 19. Command 30 can be used to make a independent 
 * PID routine. The resolution of the command is required to create a PID 
 * routine using any microcontroller or PC used to drive RoboClaw.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_read_speed_M2_highres(RoboClawRef rc, uint32_t * speed, uint32_t * direction) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 31; // command

	// send 2, expect 6
	if ((result = roboclaw_trx(rc, 2, 6)) < 0)
		return result;

	*speed = roboclaw_unpack32(rc->swap);
	*direction = rc->swap[4];

	return 0;
}

/*----------------------------------------------------------------------------
 * Reset Quadrature Encoder Values.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_reset_encoders(RoboClawRef rc) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 20; // command
	rc->swap[2] = roboclaw_checksum(rc, 2);

	// send 3
	if ((result = roboclaw_write(rc, 3)) < 0)
		return result;

	return 0;
}

/*----------------------------------------------------------------------------
 * Drive M1 with signed duty cycle
 * The duty value is signed and the range is +-1500.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_duty_cycle_M1(RoboClawRef rc, uint16_t signed_duty) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 32; // command
	roboclaw_pack16(rc->swap + 2, 1, signed_duty);
	rc->swap[4] = roboclaw_checksum(rc, 4);

	// send 5
	if ((result = roboclaw_write(rc, 5)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Drive M1 with signed duty cycle
 * The duty value is signed and the range is +-1500.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_duty_cycle_M2(RoboClawRef rc, uint16_t signed_duty) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 32; // command
	roboclaw_pack16(rc->swap + 2, 1, signed_duty);
	rc->swap[4] = roboclaw_checksum(rc, 4);

	// send 5
	if ((result = roboclaw_write(rc, 5)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Mix drive M1 and M2 with signed duty cycle
 * The duty value is signed and the range is +-1500.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_duty_cycle(RoboClawRef rc, uint16_t signed_duty_m1, uint16_t signed_duty_m2) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 34; // command
	roboclaw_pack16(rc->swap + 2, 2, signed_duty_m1, signed_duty_m2);
	rc->swap[6] = roboclaw_checksum(rc, 6);

	// send 7
	if ((result = roboclaw_write(rc, 7)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Drive M1 with signed speed
 * Drive M1 using a speed value. The sign indicates which direction the motor 
 * will turn. This command is used to drive the motor by quad pulses per second. 
 * Different quadrature encoders will have different rates at which they generate 
 * the incoming pulses. The values used will differ from one encoder to another. 
 * Once a value is sent the motor will begin to accelerate as fast as possible until
 * the defined rate is reached. 
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_speed_M1(RoboClawRef rc, uint32_t signed_speed) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 35; // command
	roboclaw_pack32(rc->swap + 2, 1, signed_speed);
	rc->swap[6] = roboclaw_checksum(rc, 6);

	// send 7
	if ((result = roboclaw_write(rc, 7)) < 0)
		return result;

}

/*----------------------------------------------------------------------------
 * Drive M2 with signed speed
 * Drive M2 using a speed value. The sign indicates which direction the motor 
 * will turn. This command is used to drive the motor by quad pulses per second. 
 * Different quadrature encoders will have different rates at which they generate 
 * the incoming pulses. The values used will differ from one encoder to another. 
 * Once a value is sent the motor will begin to accelerate as fast as possible until
 * the defined rate is reached. 
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_speed_M2(RoboClawRef rc, uint32_t signed_speed) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 36; // command
	roboclaw_pack32(rc->swap + 2, 1, signed_speed);
	rc->swap[6] = roboclaw_checksum(rc, 6);

	// send 7
	if ((result = roboclaw_write(rc, 7)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Mix Mode Drive M1/M2 with Signed speed
 * Drive M1 and M2 in the same command using a signed speed value. The sign
 * indicates which direction the motor will turn. This command is used to
 * drive both motors by quad pulses per second. Different quadrature encoders
 * will have different rates at which they generate the incoming pulses. The
 * values used will differ from one encoder to another. Once a value is sent
 * the motor will begin to accelerate as fast as possible until the rate
 * defined is reached.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_speed(RoboClawRef rc, uint32_t signed_speed_m1, uint32_t signed_speed_m2) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 37; // command
	roboclaw_pack32(rc->swap + 2, 2, signed_speed_m1, signed_speed_m2);
	rc->swap[10] = roboclaw_checksum(rc, 10);

	// send 11
	if ((result = roboclaw_write(rc, 11)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Drive M1 With Signed Speed And Acceleration 
 * Drive M1 with a signed speed and acceleration value. The sign indicates
 * which direction the motor will run. The acceleration values are not signed.
 * This command is used to drive the motor by quad pulses per second and using
 * an acceleration value for ramping. Different quadrature encoders will have
 * different rates at which they generate the incoming pulses. The values used
 * will differ from one encoder to another. Once a value is sent the motor
 * will begin to accelerate incrementally until the rate defined is reached.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_accel_speed_M1(RoboClawRef rc, uint32_t accel, uint32_t qspeed) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 38; // command
	roboclaw_pack32(rc->swap + 2, 2, accel, qspeed);
	rc->swap[10] = roboclaw_checksum(rc, 10);

	// send 11
	if ((result = roboclaw_write(rc, 11)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Drive M2 With Signed Speed And Acceleration 
 * Drive M2 with a signed speed and acceleration value. The sign indicates
 * which direction the motor will run. The acceleration values are not signed.
 * This command is used to drive the motor by quad pulses per second and using
 * an acceleration value for ramping. Different quadrature encoders will have
 * different rates at which they generate the incoming pulses. The values used
 * will differ from one encoder to another. Once a value is sent the motor
 * will begin to accelerate incrementally until the rate defined is reached.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_accel_speed_M2(RoboClawRef rc, uint32_t accel, uint32_t qspeed) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 39; // command
	roboclaw_pack32(rc->swap + 2, 2, accel, qspeed);
	rc->swap[10] = roboclaw_checksum(rc, 10);

	// send 11
	if ((result = roboclaw_write(rc, 11)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Mix Mode Drive M1 / M2 With Signed Speed And Acceleration
 * Drive M1 and M2 in the same command using one value for acceleration and
 * two signed speed values for each motor. The sign indicates which direction
 * the motor will run. The acceleration value is not signed. The motors are
 * sync during acceleration. This command is used to drive the motor by quad
 * pulses per second and using an acceleration value for ramping. Different
 * quadrature encoders will have different rates at which they generate the
 * incoming pulses. The values used will differ from one encoder to another.
 * Once a value is sent the motor will begin to accelerate incrementally until
 * the rate defined is reached.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_accel_speed(RoboClawRef rc, uint32_t accel, uint32_t qspeed_m1, uint32_t qspeed_m2) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 40; // command
	roboclaw_pack32(rc->swap + 2, 3, accel, qspeed_m1, qspeed_m2);
	rc->swap[14] = roboclaw_checksum(rc, 14);

	// send 15
	if ((result = roboclaw_write(rc, 15)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Buffered M1 Drive With Signed Speed And Distance
 * Drive M1 with a signed speed and distance value. The sign indicates which
 * direction the motor will run. The distance value is not signed. This command
 * is buffered. This command is used to control the top speed and total distance
 * traveled by the motor. Each motor channel M1 and M2 have separate buffers.
 * This command will execute immediately if no other command for that channel is
 * executing, otherwise the command will be buffered in the order it was sent.
 * Any buffered or executing command can be stopped when a new command is issued
 * by setting the Buffer argument. All values used are in quad pulses per second.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_speed_dist_M1(RoboClawRef rc, uint32_t qspeed, uint32_t dist, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 41; // command
	roboclaw_pack32(rc->swap + 2, 2, qspeed, dist);
	rc->swap[10] = buffer;
	rc->swap[11] = roboclaw_checksum(rc, 11);

	// send 12
	if ((result = roboclaw_write(rc, 12)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Buffered M2 Drive With Signed Speed And Distance
 * Drive M2 with a signed speed and distance value. The sign indicates which
 * direction the motor will run. The distance value is not signed. This command
 * is buffered. This command is used to control the top speed and total distance
 * traveled by the motor. Each motor channel M1 and M2 have separate buffers.
 * This command will execute immediately if no other command for that channel is
 * executing, otherwise the command will be buffered in the order it was sent.
 * Any buffered or executing command can be stopped when a new command is issued
 * by setting the Buffer argument. All values used are in quad pulses per second.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_speed_dist_M2(RoboClawRef rc, uint32_t qspeed, uint32_t dist, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 42; // command
	roboclaw_pack32(rc->swap + 2, 2, qspeed, dist);
	rc->swap[10] = buffer;
	rc->swap[11] = roboclaw_checksum(rc, 11);

	// send 12
	if ((result = roboclaw_write(rc, 12)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Buffered Mix Mode Drive M1 / M2 With Signed Speed And Distance
 * Drive M1 and M2 with a speed and distance value. The sign indicates which
 * direction the motor will run. The distance value is not signed. This
 * command is buffered. Each motor channel M1 and M2 have separate buffers.
 * This command will execute immediately if no other command for that channel
 * is executing, otherwise the command will be buffered in the order it was
 * sent. Any buffered or executing command can be stopped when a new command
 * is issued by setting the Buffer argument. All values used are in quad
 * pulses per second.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_speed_dist(RoboClawRef rc, uint32_t qspeed_m1, uint32_t dist_m1, uint32_t qspeed_m2, uint32_t dist_m2, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 43; // command
	roboclaw_pack32(rc->swap + 2, 4, qspeed_m1, dist_m1, qspeed_m2, dist_m2);
	rc->swap[18] = buffer;
	rc->swap[19] = roboclaw_checksum(rc, 19);

	// send 20
	if ((result = roboclaw_write(rc, 20)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Buffered M1 Drive With Signed Speed, Accel And Distance
 * Drive M1 with a speed, acceleration and distance value. The sign indicates
 * which direction the motor will run. The acceleration and distance values
 * are not signed. This command is used to control the motors top speed, total
 * distanced traveled and at what incremental acceleration value to use until
 * the top speed is reached. Each motor channel M1 and M2 have separate
 * buffers. This command will execute immediately if no other command for that
 * channel is executing, otherwise the command will be buffered in the order
 * it was sent. Any buffered or executing command can be stopped when a new
 * command is issued by setting the Buffer argument. All values used are in
 * quad pulses per second.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_accel_speed_dist_M1(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t dist, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 44; // command
	roboclaw_pack32(rc->swap + 2, 3, accel, qspeed, dist);
	rc->swap[14] = buffer;
	rc->swap[15] = roboclaw_checksum(rc, 15);

	// send 16
	if ((result = roboclaw_write(rc, 16)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Buffered M2 Drive With Signed Speed, Accel And Distance
 * Drive M2 with a speed, acceleration and distance value. The sign indicates
 * which direction the motor will run. The acceleration and distance values
 * are not signed. This command is used to control the motors top speed, total
 * distanced traveled and at what incremental acceleration value to use until
 * the top speed is reached. Each motor channel M1 and M2 have separate
 * buffers. This command will execute immediately if no other command for that
 * channel is executing, otherwise the command will be buffered in the order
 * it was sent. Any buffered or executing command can be stopped when a new
 * command is issued by setting the Buffer argument. All values used are in
 * quad pulses per second.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_accel_speed_dist_M2(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t dist, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 45; // command
	roboclaw_pack32(rc->swap + 2, 3, accel, qspeed, dist);
	rc->swap[14] = buffer;
	rc->swap[15] = roboclaw_checksum(rc, 15);

	// send 16
	if ((result = roboclaw_write(rc, 16)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Buffered Mix Mode Drive M1 / M2 With Signed Speed, Accel And Distance
 * Drive M1 and M2 with a speed, acceleration and distance value. The sign
 * indicates which direction the motor will run. The acceleration and distance
 * values are not signed. This command is used to control both motors top
 * speed, total distanced traveled and at what incremental acceleration value
 * to use until the top speed is reached. Each motor channel M1 and M2 have
 * separate buffers. This command will execute immediately if no other command
 * for that channel is executing, otherwise the command will be buffered in
 * the order it was sent. Any buffered or executing command can be stopped
 * when a new command is issued by setting the Buffer argument. All values
 * used are in quad pulses per second.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_accel_speed_dist(RoboClawRef rc, uint32_t accel, uint32_t qspeed_m1, uint32_t dist_m1, uint32_t qspeed_m2, uint32_t dist_m2, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 46; // command
	roboclaw_pack32(rc->swap + 2, 5, accel, qspeed_m1, dist_m1, qspeed_m2, dist_m2);
	rc->swap[22] = buffer;
	rc->swap[23] = roboclaw_checksum(rc, 23);

	// send 24
	if ((result = roboclaw_write(rc, 24)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Read Buffer Length
 * Read both motor M1 and M2 buffer lengths. This command can be used to
 * determine how many commands are waiting to execute.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_read_buffer_length(RoboClawRef rc, uint8_t * buffer_m1, uint8_t * buffer_m2) 
{
	assert(rc);
	int result;

	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 47; // command

	// send 2, expect 3
	if ((result = roboclaw_trx(rc, 2, 3)) < 0)
		return result;

	*buffer_m1 = rc->swap[0];
	*buffer_m2 = rc->swap[2];

	return 0;
}

/*----------------------------------------------------------------------------
 * Mix Mode Drive M1 / M2 With Signed Speed And Individual Accelerations
 * Drive M1 and M2 in the same command using one value for acceleration and
 * two signed speed values for each motor. The sign indicates which direction
 * the motor will run. The acceleration value is not signed. The motors are
 * sync during acceleration. This command is used to drive the motor by quad
 * pulses per second and using an acceleration value for ramping. Different
 * quadrature encoders will have different rates at which they generate the
 * incoming pulses. The values used will differ from one encoder to another.
 * Once a value is sent the motor will begin to accelerate incrementally until
 * the rate defined is reached.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_speed_individual_accel(RoboClawRef rc, uint32_t accel_m1, uint32_t qspeed_m1, uint32_t accel_m2, uint32_t qspeed_m2)
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 50; // command
	roboclaw_pack32(rc->swap + 2, 4, accel_m1, qspeed_m1, accel_m2, qspeed_m2);
	rc->swap[18] = roboclaw_checksum(rc, 18);

	// send 19
	if ((result = roboclaw_write(rc, 19)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Buffered Mix Mode Drive M1 / M2 With Signed Speed, Individual Accel And Distance
 * Drive M1 and M2 with a speed, acceleration and distance value. The sign
 * indicates which direction the motor will run. The acceleration and distance
 * values are not signed. This command is used to control both motors top
 * speed, total distanced traveled and at what incremental acceleration value
 * to use until the top speed is reached. Each motor channel M1 and M2 have
 * separate buffers. This command will execute immediately if no other command
 * for that channel is executing, otherwise the command will be buffered in
 * the order it was sent. Any buffered or executing command can be stopped
 * when a new command is issued by setting the Buffer argument. All values
 * used are in quad pulses per second. The Buffer argument can be set to a 1
 * or 0. If a value of 0 is used the command will be buffered and executed in
 * the order sent. If a value of 1 is used the current running command is
 * stopped, any other commands in the buffer are deleted and the new command
 * is executed.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_speed_individual_accel_dist(RoboClawRef rc, uint32_t accel_m1, uint32_t qspeed_m1, uint32_t dist_m1, uint32_t accel_m2, uint32_t qspeed_m2, uint32_t dist_m2, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 51; // command
	roboclaw_pack32(rc->swap + 2, 6, accel_m1, qspeed_m1, dist_m1, accel_m2, qspeed_m2, dist_m2);
	rc->swap[26] = buffer;
	rc->swap[27] = roboclaw_checksum(rc, 27);

	// send 28
	if ((result = roboclaw_write(rc, 28)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Drive M1 With Signed Duty And Acceleration
 * Drive M1 with a signed duty and acceleration value. The sign indicates
 * which direction the motor will run. The acceleration values are not signed.
 * This command is used to drive the motor by PWM and using an acceleration
 * value for ramping. Accel is the rate per second at which the duty changes
 * from the current duty to the specified duty.
 * The duty value is signed and the range is +-1500.
 * The accel value range is 0 to 65535
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_duty_accel_M1(RoboClawRef rc, uint16_t signed_duty, uint16_t accel) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 52; // command
	roboclaw_pack16(rc->swap + 2, 2, signed_duty, accel);
	rc->swap[6] = roboclaw_checksum(rc, 6);

	// send 7
	if ((result = roboclaw_write(rc, 7)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Drive M2 With Signed Duty And Acceleration
 * Drive M2 with a signed duty and acceleration value. The sign indicates
 * which direction the motor will run. The acceleration values are not signed.
 * This command is used to drive the motor by PWM and using an acceleration
 * value for ramping. Accel is the rate per second at which the duty changes
 * from the current duty to the specified duty.
 * The duty value is signed and the range is +-1500.
 * The accel value range is 0 to 65535
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_duty_accel_M2(RoboClawRef rc, uint16_t signed_duty, uint16_t accel) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 53; // command
	roboclaw_pack16(rc->swap + 2, 2, signed_duty, accel);
	rc->swap[6] = roboclaw_checksum(rc, 6);

	// send 7
	if ((result = roboclaw_write(rc, 7)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Mix Mode Drive M1 / M2 With Signed Duty And Acceleration
 * Drive M1 and M2 in the same command using acceleration and duty values for
 * each motor. The sign indicates which direction the motor will run. The
 * acceleration value is not signed. This command is used to drive the motor
 * by PWM using an acceleration value for ramping.
 * The duty value is signed and the range is +-1500.
 * The accel value range is 0 to 65535
 * TODO: Documentation might have an error, accel values might be 2 bytes instead of 4.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_duty_accel(RoboClawRef rc, uint16_t signed_duty_m1, uint32_t accel_m1, uint16_t signed_duty_m2, uint32_t accel_m2) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 54; // command
	roboclaw_pack16(rc->swap + 2, 1, signed_duty_m1);
	roboclaw_pack32(rc->swap + 4, 1, accel_m1);
	roboclaw_pack16(rc->swap + 8, 1, signed_duty_m2);
	roboclaw_pack32(rc->swap + 10, 1, accel_m2);
	rc->swap[14] = roboclaw_checksum(rc, 14);

	// send 15
	if ((result = roboclaw_write(rc, 15)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Set Motor 1 Position PID Constants
 * The RoboClaw Position PID system consist of seven constants starting with P
 * = Proportional, I= Integral and D= Derivative, MaxI = Maximum Integral
 * windup, Deadzone in encoder counts, MinPos = Minimum Position and MaxPos =
 * Maximum Position. The defaults values are all zero.
 * TODO: The documentation might have an error, I used a checksum here but the
 * docs don't say anything about it.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_set_pos_PID_M1(RoboClawRef rc, uint32_t p, uint32_t i, uint32_t d, uint32_t max_i, uint32_t deadzone, uint32_t min_pos, uint32_t max_pos) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 61; // command
	roboclaw_pack32(rc->swap + 2, 7, p, i, d, max_i, deadzone, min_pos, max_pos);
	rc->swap[30] = roboclaw_checksum(rc, 30);

	// send 31
	if ((result = roboclaw_write(rc, 31)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Set Motor 2 Position PID Constants
 * The RoboClaw Position PID system consist of seven constants starting with P
 * = Proportional, I= Integral and D= Derivative, MaxI = Maximum Integral
 * windup, Deadzone in encoder counts, MinPos = Minimum Position and MaxPos =
 * Maximum Position. The defaults values are all zero.
 * TODO: The documentation might have an error, I used a checksum here but the
 * docs don't say anything about it.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_set_pos_PID_M2(RoboClawRef rc, uint32_t p, uint32_t i, uint32_t d, uint32_t max_i, uint32_t deadzone, uint32_t min_pos, uint32_t max_pos) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 62; // command
	roboclaw_pack32(rc->swap + 2, 7, p, i, d, max_i, deadzone, min_pos, max_pos);
	rc->swap[30] = roboclaw_checksum(rc, 30);

	// send 31
	if ((result = roboclaw_write(rc, 31)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Drive M1 with signed Speed, Accel, Deccel and Position
 * Move M1 position from the current position to the specified new position
 * and hold the new position. Accel sets the acceleration value and deccel the
 * decceleration value. QSpeed sets the speed in quadrature pulses the motor
 * will run at after acceleration and before decceleration.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_accel_speed_deccel_pos_M1(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t decel, uint32_t pos, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 65; // command
	roboclaw_pack32(rc->swap + 2, 4, accel, qspeed, decel, pos);
	rc->swap[18] = buffer;
	rc->swap[19] = roboclaw_checksum(rc, 19);

	// send 20
	if ((result = roboclaw_write(rc, 20)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Drive M1 with signed Speed, Accel, Deccel and Position
 * Move M1 position from the current position to the specified new position
 * and hold the new position. Accel sets the acceleration value and deccel the
 * decceleration value. QSpeed sets the speed in quadrature pulses the motor
 * will run at after acceleration and before decceleration.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_accel_speed_deccel_pos_M2(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t decel, uint32_t pos, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 66; // command
	roboclaw_pack32(rc->swap + 2, 4, accel, qspeed, decel, pos);
	rc->swap[18] = buffer;
	rc->swap[19] = roboclaw_checksum(rc, 19);

	// send 20
	if ((result = roboclaw_write(rc, 20)) < 0)
		return result;
}

/*----------------------------------------------------------------------------
 * Drive M1 & M2 with signed Speed, Accel, Deccel and Position
 * Move M1 & M2 positions from their current positions to the specified new
 * positions and hold the new positions. Accel sets the acceleration value and
 * deccel the decceleration value. QSpeed sets the speed in quadrature pulses
 * the motor will run at after acceleration and before decceleration.
 * Return: 0: Success.
 * Return < 0: Error.
 */
int roboclaw_drive_accel_speed_deccel_pos(RoboClawRef rc, uint32_t accel, uint32_t qspeed, uint32_t decel, uint32_t pos, uint8_t buffer) 
{
	assert(rc);
	int result;

 	// command
	rc->swap[0] = rc->address; // address
	rc->swap[1] = 67; // command
	roboclaw_pack32(rc->swap + 2, 4, accel, qspeed, decel, pos);
	rc->swap[18] = buffer;
	rc->swap[19] = roboclaw_checksum(rc, 19);

	// send 20
	if ((result = roboclaw_write(rc, 20)) < 0)
		return result;
}