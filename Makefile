#
# Makefile example for libroboclaw
#
# Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
# George Mason University - Autonomous Robotics Laboratory
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# flags
CC:=gcc
LD:=${CC}
AR:=ar -ruv
CFLAGS:=-fPIC -pipe -g

# libroboclaw
ROBOCLAW_SRCS:=roboclaw.c
ROBOCLAW_OBJS:=roboclaw.o
ROBOCLAW_SO:=libroboclaw.so
ROBOCLAW_STATICLIB:=libroboclaw.a

#------------------------------------------------------------------------------

all: $(ROBOCLAW_SO) $(ROBOCLAW_STATICLIB)

clean:
	rm -rf $(ROBOCLAW_OBJS) 
	rm -rf $(ROBOCLAW_SO) $(ROBOCLAW_STATICLIB)
	rm -rf Dependencies *.d *~

# libroboclaw (shared library)
$(ROBOCLAW_SO) : $(ROBOCLAW_OBJS)
	$(LD) -shared $^ -o $@  

# libroboclaw (static library archive)
$(ROBOCLAW_STATICLIB) : $(ROBOCLAW_OBJS)
	$(AR) $@ $^

# making object files from .c files in general
%.o : %.c
	$(CC) $(CFLAGS) -MMD -c $< -o $@
	@cat $*.d >> Dependencies
	@rm -f $*.d

-include Dependencies