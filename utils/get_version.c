/**
 * get_version
 * Example program to get and display version string from the RoboClaw
 *
 * Copyright 2010 - Christopher Vo (cvo1@cs.gmu.edu)
 * George Mason University - Autonomous Robotics Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "roboclaw.h"
#include <unistd.h>

RoboClawInstance rc;

int main(int argc, char *argv[])
{
	// open -------------------------------------------------------------------
	int ret = roboclaw_init(&rc, 0x80, 38400, "/dev/ttyUSB0");
	if (ret < 0) {
		fprintf(stderr, "Error calling roboclaw_init.");
		exit(-1);
	}

	// get version
	char result[32];
	memset(result, 0, 32);
	ret = roboclaw_version(&rc, result);
	if (ret < 0) {
		fprintf(stderr, "Error calling roboclaw_version.");
	}

	// print the version
	printf("version: %s\n", result);


	roboclaw_drive_forward_M1(&rc, 0);

	// close ------------------------------------------------------------------
	ret = roboclaw_close(&rc);
	if (ret < 0)
		fprintf(stderr, "Couldn't close roboclaw instance.");

	return 0;
}